CHANGELOG
=========

2020-01-22 (v1.0.1)
-------------------
- Allow for netsquid 0.7

2019-06-26
----------
- Created this snippet
