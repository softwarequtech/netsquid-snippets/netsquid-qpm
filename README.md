NetSquid-QMM (1.0.1)
========================

This repository provides a Quantum Memory Manager for NetSquid. It also
provides a specialised Communication Memory Manager for communication devices
that distinguish between storage and communication qubits.

Description
-----------

This is a user contributed _snippet_ for the [NetSquid quantum network
simulator](https://netsquid.org).

This Snippet provides a memory manager that can be used to administer and
arbitrate access to a quantum node's memory.

Installation
------------

See the [INSTALL file](INSTALL.md) for instruction of how to install this snippet.

Usage
-----

Create a unique instance per `Node` and add as a sub-component

```
node.add_component(
    QuantumMemoryManager(num_qubits), 
    "qmm",
)

node.add_component(
    CommMemoryManager(num_qubits, communication_qubit_ids),
    "comm-qmm",
)
```

Contributors
------------

This snippet has been developed by:
- Axel Dahlberg <E.A.Dahlberg@tudelft.nl>
- Matt Skrzypczyk <M.D.Skrzypczyk@student.tudelft.nl>
- Wojciech Kozlowski <W.Kozlowski@tudelft.nl>

License
-------

The NetSquid-QMM has the following license:

> Copyright 2018 QuTech (TUDelft and TNO)
> 
>   Licensed under the Apache License, Version 2.0 (the "License");
>   you may not use this file except in compliance with the License.
>   You may obtain a copy of the License at
> 
>     http://www.apache.org/licenses/LICENSE-2.0
> 
>   Unless required by applicable law or agreed to in writing, software
>   distributed under the License is distributed on an "AS IS" BASIS,
>   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
>   See the License for the specific language governing permissions and
>   limitations under the License.
