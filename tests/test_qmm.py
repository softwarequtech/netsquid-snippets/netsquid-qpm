"""Unit tests for the sample module.

This file will be discovered by ``python setup.py test``.

"""
import unittest
import unittest.mock as mock

from netsquid_qmm.qmm import (
    QuantumMemoryManager,
    CommMemoryManager,
)


class TestQuantumMemoryManager(unittest.TestCase):
    """Unit tests for Quantum Memory Manager.

    """

    def setUp(self):
        # Create a manager for 5 qubits.
        self.size = 5
        self.qmm = QuantumMemoryManager(self.size)

    def tearDown(self):
        pass

    def test_reservation(self):
        """Test the ID reservation mechanism."""

        # Will be working with qubit ID = 1
        qid = 1

        # Reserve qubit
        self.qmm.reserve_qubit_id(qid)

        # Try to reserve qubit, should raise an exception
        with self.assertRaises(ValueError):
            self.qmm.reserve_qubit_id(qid)

        # Vacate the qubit
        self.qmm.vacate_qubit(qid)

        # Make sure double free doesn't do anything bad
        self.qmm.vacate_qubit(qid)

        # Try to reserve again, should not raise exception
        self.qmm.reserve_qubit_id(qid)

        # Reserve all remaining qubits
        for _ in range(self.qmm.size-1):
            self.assertNotEqual(
                self.qmm.reserve_qubit(),
                QuantumMemoryManager.NO_FREE_QUBIT,
            )

        # The next reservation should return -1
        self.assertEqual(
            self.qmm.reserve_qubit(),
            QuantumMemoryManager.NO_FREE_QUBIT,
        )

    def test_out_of_bounds(self):
        """Test attempts to reserve out of bounds memory."""

        # Will be working with a qubit beyond the memory range
        qid = self.qmm.size + 1

        # Try to reserve qubit, should raise an IndexError
        with self.assertRaises(IndexError):
            self.qmm.reserve_qubit_id(qid)

        # Try to vacate a qubit, should also raise
        with self.assertRaises(IndexError):
            self.qmm.vacate_qubit(qid)


class TestCommMemoryManager(unittest.TestCase):
    """Unit tests for Communication Memory Manager.

    """

    def setUp(self):
        # Create a manager for 5 qubits.
        self.size = 5
        self.comm_ids = [1, 3]
        self.qmm = CommMemoryManager(self.size, self.comm_ids)

        self.comm_ids = set(self.comm_ids)
        self.storage_ids = set(range(self.size)) - self.comm_ids

    def tearDown(self):
        pass

    def verify_memory(self, comm_ids, storage_ids):
        """Verify that the memory manager's idea of free memory matches the
        given sets.

        Parameters
        ----------
        comm_ids : set of ints
            The expected free communications qubit IDs.
        storage_ids : set of ints
            The expected free storage qubit IDs.
        """
        self.assertSetEqual(
            comm_ids,
            set(self.qmm.get_free_communication_ids()),
        )
        self.assertSetEqual(
            storage_ids,
            set(self.qmm.get_free_storage_ids()),
        )
        self.assertTupleEqual(
            (len(comm_ids), len(storage_ids)),
            self.qmm.get_free_mem_ad(),
        )

    def test_reservation(self):
        """Test basic reservations."""

        comm_ids = self.comm_ids
        storage_ids = self.storage_ids

        # Verify that the free storage is as we expect it
        self.verify_memory(comm_ids, storage_ids)

        # Try reserving a communication qubit
        comm_q = self.qmm.reserve_communication_qubit()
        self.assertIn(comm_q, self.comm_ids)

        # Try reserving a storage qubit
        storage_q = self.qmm.reserve_storage_qubit()
        self.assertIn(storage_q, self.storage_ids)

        # Update free ids for the test
        comm_ids = comm_ids - set([comm_q])
        storage_ids = storage_ids - set([storage_q])

        # Verify that the free storage is as we expect it
        self.verify_memory(comm_ids, storage_ids)

        # Reserve them both as a pair - should have space to reserve another
        # pair
        comm_q, storage_q = self.qmm.reserve_entanglement_pair()
        self.assertIn(comm_q, self.comm_ids)
        self.assertIn(storage_q, self.storage_ids)

        # Update free ids for the test
        comm_ids = comm_ids - set([comm_q])
        storage_ids = storage_ids - set([storage_q])

        # Verify that the free storage is as we expect it
        self.verify_memory(comm_ids, storage_ids)

        # Free the qubits from the last reservation
        self.qmm.vacate_qubit(comm_q)
        self.qmm.vacate_qubit(storage_q)

        # Verify free storage after the free operations
        comm_ids = comm_ids | set([comm_q])
        storage_ids = storage_ids | set([storage_q])

        # Verify that the free storage is as we expect it
        self.verify_memory(comm_ids, storage_ids)

    def test_callback(self):
        """Test behaviour of communication qubit callback."""

        # Reserve an entanglement pair
        comm_1, storage_q = self.qmm.reserve_entanglement_pair()

        self.assertNotEqual(comm_1, QuantumMemoryManager.NO_FREE_QUBIT)
        self.assertNotEqual(storage_q, QuantumMemoryManager.NO_FREE_QUBIT)

        # Create a mock callback
        cbk = mock.MagicMock()

        # Try to register callback, since we still have a free communication
        # qubit, this should assert
        with self.assertRaises(AssertionError):
            self.qmm.register_comm_q_callback(cbk)

        # Reserve the next communication qubit
        comm_2 = self.qmm.reserve_communication_qubit()

        self.assertNotEqual(comm_2, QuantumMemoryManager.NO_FREE_QUBIT)

        # Register callback now
        self.qmm.register_comm_q_callback(cbk)

        # Free the storage qubit - callback should not be called
        self.qmm.vacate_qubit(storage_q)
        cbk.assert_not_called()

        # Free a comm qubit - callback should be called
        self.qmm.vacate_qubit(comm_1)
        cbk.assert_called_once()

        # Reset the mock, and free the second qubit - callback should not be
        # called
        cbk.reset_mock()
        self.qmm.vacate_qubit(comm_2)
        cbk.assert_not_called()

    def test_out_of_memory(self):
        """Verify that running out of memory is handled correctly."""

        # Reserve two entanglement pairs
        comm_1, storage_1 = self.qmm.reserve_entanglement_pair()
        comm_2, storage_2 = self.qmm.reserve_entanglement_pair()

        self.assertNotEqual(comm_1, QuantumMemoryManager.NO_FREE_QUBIT)
        self.assertNotEqual(storage_1, QuantumMemoryManager.NO_FREE_QUBIT)
        self.assertNotEqual(comm_2, QuantumMemoryManager.NO_FREE_QUBIT)
        self.assertNotEqual(storage_2, QuantumMemoryManager.NO_FREE_QUBIT)

        # Check that there are no more free comm qubits, but there are free
        # storage qubits
        self.assertEqual(0, len(self.qmm.get_free_communication_ids()))
        self.assertGreater(len(self.qmm.get_free_storage_ids()), 0)

        # Try to reserve a communication qubit
        self.assertEqual(
            self.qmm.reserve_communication_qubit(),
            QuantumMemoryManager.NO_FREE_QUBIT,
        )

        # Make sure reserving a pair also fails and the amount of free storage
        # ids does not change
        storage_ids = set(self.qmm.get_free_storage_ids())

        self.assertEqual(
            self.qmm.reserve_entanglement_pair(),
            (QuantumMemoryManager.NO_FREE_QUBIT,
             QuantumMemoryManager.NO_FREE_QUBIT),
        )

        self.assertSetEqual(storage_ids, set(self.qmm.get_free_storage_ids()))

        # Free a communication qubit (but not storage), and reserve another
        # pair
        self.qmm.vacate_qubit(comm_1)

        comm_1, storage_3 = self.qmm.reserve_entanglement_pair()

        self.assertNotEqual(comm_1, QuantumMemoryManager.NO_FREE_QUBIT)
        self.assertNotEqual(storage_3, QuantumMemoryManager.NO_FREE_QUBIT)

        # Check that we no longer have free storage qubits
        self.assertEqual(0, len(self.qmm.get_free_storage_ids()))

        # Try to reserve a storage qubit
        self.assertEqual(
            self.qmm.reserve_storage_qubit(),
            QuantumMemoryManager.NO_FREE_QUBIT,
        )

        # Free a communication qubit
        self.qmm.vacate_qubit(comm_2)

        # Try to reserve an entanglement pair - it will fail as there are not
        # storage qubits - make sure the amount of free comm qubits does not
        # change
        comm_ids = set(self.qmm.get_free_communication_ids())

        self.assertEqual(
            self.qmm.reserve_entanglement_pair(),
            (QuantumMemoryManager.NO_FREE_QUBIT,
             QuantumMemoryManager.NO_FREE_QUBIT),
        )

        self.assertSetEqual(
            comm_ids,
            set(self.qmm.get_free_communication_ids()),
        )


if __name__ == "__main__":  # pragma: no cover
    unittest.main(verbosity=2)
