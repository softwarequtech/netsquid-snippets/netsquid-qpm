"""Quantum Memory Manager snippet for NetSquid.
"""
import queue

from netsquid.components.component import Component


class QuantumMemoryManager(Component):
    """Quantum Memory Manager"""

    NO_FREE_QUBIT = -1

    def __init__(self, size):
        """Constructor.

        Parameters
        ----------
        size : int
            Number of qubits in memory to manage.
        """
        super().__init__("Quantum Memory Manager")
        self._size = size
        self.reserved_qubits = [False] * size

    @property
    def size(self):
        """
        Returns
        -------
        int
            Size of quantum memory.
        """
        return self._size

    def reserve_qubit(self):
        """Reserve a qubit.

        Returns
        -------
        :return: int
            The ID of the reserved qubit. -1 if there is no available qubit.
        """
        free_qubits = filter(
            lambda qid: not self.reserved_qubits[qid], range(self.size)
        )

        try:
            qid = next(free_qubits)
            self.reserve_qubit_id(qid)
            return qid

        except StopIteration:
            return QuantumMemoryManager.NO_FREE_QUBIT

    def reserve_qubit_id(self, qid):
        """Reserve a particular qubit.

        Parameters
        ----------
        qid : int
            The qubit ID to reserve.
        """
        if qid >= len(self.reserved_qubits):
            raise IndexError("Qubit ID {} out of range!".format(qid))

        if self.reserved_qubits[qid]:
            raise ValueError("Qubit {} already reserved!".format(qid))

        self.reserved_qubits[qid] = True

    def vacate_qubit(self, qid):
        """Frees a qubit. If a qubit was already free nothing happens.

        Parameters
        ----------
        qid : int
            The qubit ID to free.
        """
        if qid >= len(self.reserved_qubits):
            raise IndexError("Qubit ID {} out of range!".format(qid))

        self.reserved_qubits[qid] = False


class CommMemoryManager(QuantumMemoryManager):
    """Quantum Memory manager specialisation for communication devices with
    dedicated communication qubits"""

    def __init__(self, size, comm_qubit_ids):
        """Constructor.

        Parameters
        ----------
        size : int
            Number of qubits in memory to manage.
        comm_qubit_ids : list of ints
            IDs of qubits to be used as communication qubits.
        """
        super().__init__(size)

        self.comm_qubits = set(comm_qubit_ids)
        self.storage_qubits = (set(range(self.size)) - self.comm_qubits)

        self.comm_queue = queue.Queue()

    def vacate_qubit(self, qid):
        """Frees a qubit. If a qubit was already free nothing happens. If a
        callback has been registered to receive a notification about a free
        communication qubit, it will be called.

        Parameters
        ----------
        qid : int
            The qubit ID to free.
        """
        super().vacate_qubit(qid)

        if not self.comm_queue.empty() and qid in self.comm_qubits:
            cbk = self.comm_queue.get()
            cbk()

    def reserve_communication_qubit(self):
        """Reserve a communication qubit.

        Returns
        -------
        :return: int
            The ID of the reserved qubit. -1 if there is no available qubit.
        """
        free_comms = self._get_free_communication_iter()

        try:
            comm_q = next(free_comms)
            self.reserve_qubit_id(comm_q)
            return comm_q

        except StopIteration:
            return QuantumMemoryManager.NO_FREE_QUBIT

    def register_comm_q_callback(self, cbk):
        """Register a callback that will be used to notify the caller when the
        communication qubit becomes available. Should not be used if a
        communication qubit is available.

        Prameters
        ---------
        cbk : function()
            The callback function
        """
        assert not self.get_free_communication_ids()
        self.comm_queue.put(cbk)

    def reserve_storage_qubit(self):
        """Reserve a non-communication qubit.

        Returns
        -------
        int
            The ID of the reserved qubit. -1 if there is no available qubit.
        """
        free_storage = self._get_free_storage_iter()

        try:
            storage_q = next(free_storage)
            self.reserve_qubit_id(storage_q)
            return storage_q

        except StopIteration:
            return QuantumMemoryManager.NO_FREE_QUBIT

    def reserve_entanglement_pair(self):
        """Reserves a pair of communication qubit and storage qubits.

        Returns
        -------
        comm_q : int
            The ID of the reserved communication qubit. -1 if there is no
            available qubit.
        storage_q : int
            The ID of the reserved storage qubit. -1 if there is no available
            qubit.
        """
        # Obtain a communication qubit
        comm_q = self.reserve_communication_qubit()

        if comm_q == QuantumMemoryManager.NO_FREE_QUBIT:
            return (QuantumMemoryManager.NO_FREE_QUBIT,) * 2

        # Obtain a storage qubit
        storage_q = self.reserve_storage_qubit()

        if storage_q == QuantumMemoryManager.NO_FREE_QUBIT:
            self.vacate_qubit(comm_q)
            return (QuantumMemoryManager.NO_FREE_QUBIT,) * 2

        return comm_q, storage_q

    def _get_free_communication_iter(self):
        """Get the iterator for free communication qubits.

        Returns
        -------
        iterator of ints
            Iterator over the available communication qubits.
        """
        return filter(
            lambda qid: not self.reserved_qubits[qid],
            self.comm_qubits,
        )

    def get_free_communication_ids(self):
        """Get the IDs of all free communication qubits.

        Returns
        -------
        list of ints
            List of the available communication qubits.
        """
        return list(self._get_free_communication_iter())

    def _get_free_storage_iter(self):
        """Get the iterator for free storage qubits.

        Returns
        -------
        iterator of ints
            Iterator over the available storage qubits.
        """
        return filter(
            lambda qid: not self.reserved_qubits[qid],
            self.storage_qubits,
        )

    def get_free_storage_ids(self):
        """Get the IDs of all free storage qubits.

        Returns
        -------
        list of ints
            List of the available storage qubits.
        """
        return list(self._get_free_storage_iter())

    def get_free_mem_ad(self):
        """Get the amount of free communication memory.

        Returns
        -------
        (int, int)
            A tuple of free communication qubits and storage qubits.
        """
        free_mem = (
            len(self.get_free_communication_ids()),
            len(self.get_free_storage_ids()),
        )

        return free_mem
